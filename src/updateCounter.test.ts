import * as test from 'tape';

import { updateCounter } from './updateCounter';

test('The counter should add each increment to its value', (t) => {
  t.plan(1);

  t.equal(updateCounter(41, 1), 42);
});

test('The counter should subtract negative increments from its value', (t) => {
  t.plan(1);

  t.equal(updateCounter(43, -1), 42);
});
