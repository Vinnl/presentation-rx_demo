import { Observable } from 'rxjs/Rx';

const incrementClick$ =
	Observable.fromEvent(document.getElementById('incrementButton'), 'click');
const decrementClick$ =
	Observable.fromEvent(document.getElementById('decrementButton'), 'click');

const counter$ =
	Observable.merge(
    incrementClick$.map(() => 1),
    decrementClick$.map(() => -1)
  )
	.startWith(0)
  .scan((acc, val) => acc + val);

counter$.subscribe(counter => {
  document.getElementById('counter').innerHTML = counter.toString();
});
